class CreateChildBallads < ActiveRecord::Migration[5.0]
  def change
    create_table :child_ballads do |t|
      t.integer :number
      t.string :name

      t.timestamps
    end
  end
end
