class CreateSongs < ActiveRecord::Migration[5.0]
  def change
    create_table :songs do |t|
      t.integer :ballad_id
      t.string :artist
      t.string :title
      t.string :source
      t.string :release_year
      t.string :length
      t.boolean :have

      t.timestamps
    end
  end
end
