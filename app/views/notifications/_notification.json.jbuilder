json.extract! notification, :id, :text, :expires_on, :created_at, :updated_at
json.url notification_url(notification, format: :json)
