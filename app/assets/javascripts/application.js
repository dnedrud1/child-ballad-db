// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require jquery.mCustomScrollbar.concat.min
//= require_tree .

$(function() {
  scrollPageJS();
  tableJS();
});

$(document).on('turbolinks:load', function() {
  scrollPageJS();
  tableJS();
})

var scrollPageJS = function() {
  $(".ballads.scroll").mCustomScrollbar({
    theme:"inset"
  });
  $('.ballad-link').click(function() {
    $('.ballad-link').removeClass('selected');
    $(this).addClass('selected');
    $.getScript(this.href);
    if ($(window).width() < 915) {
      $('html, body').animate({
          scrollTop: $(".ballad-songs").offset().top
      }, 200);
    }
    return false;
  })
}


var tableJS = function() {
  // front end table JS
  $(document).on("click", ".frontend #songs th a, .frontend #songs .pagination a", function() {
    $.getScript(this.href);
    return false;
  });
  $(".frontend #songs_search select").change(function() {
    $.get($(".frontend #songs_search").attr("action"), $(".frontend #songs_search").serialize(), null, "script");
    return false;
  });
  $(".frontend #songs_search input").keyup(function() {
    $.get($(".frontend #songs_search").attr("action"), $(".frontend #songs_search").serialize(), null, "script");
    return false;
  });
  // back end table JS
  $(document).on("click", ".backend #songs th a, .backend #songs .pagination a", function() {
    $.getScript(this.href);
    return false;
  });
  $(".backend #songs_search input").keyup(function() {
    $.get($(".backend #songs_search").attr("action"), $(".backend #songs_search").serialize(), null, "script");
    return false;
  });
};