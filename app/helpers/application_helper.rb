module ApplicationHelper
  def sortable(title = nil, column)
    title ||= column.titleize
    css_class = column == sort_column ? "current #{sort_direction}" : nil
    direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"
    link_to title, song_params.merge(:sort => column, :direction => direction, :page => nil), {:class => css_class}
  end

  def song_params
    params.permit(:ballad_id, :artist, :title, :source, :release_year, :length, :have, :search, :page, :sort, :direction)
  end
end
