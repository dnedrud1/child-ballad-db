class BalladImportsController < ApplicationController

  before_action :authenticate_user!
  
  def new
    @ballad_import = BalladImport.new
  end

  def create
    @ballad_import = BalladImport.new(params[:ballad_import])
    if @ballad_import.save
      render :new
    else
      render :new
    end
  end
end