class AdminPagesController < ApplicationController

  before_action :authenticate_user!
  helper_method :sort_column, :sort_direction
  
  def home
  end

  def music_data
    @ballads = ChildBallad.order(:number)
  end

  def ballad_data
    @ballad = ChildBallad.find(params[:id])
    @songs = @ballad.songs
  end

  def songs_data
    @songs = Song.search(song_params[:search]).order(sort_column + " " + sort_direction).paginate(:per_page => 50, :page => song_params[:page])
  end

  private
  
    def sort_column
      Song.column_names.include?(song_params[:sort]) ? song_params[:sort] : "ballad_id"
    end
    
    def sort_direction
      %w[asc desc].include?(song_params[:direction]) ? song_params[:direction] : "asc"
    end

    def song_params
      params.permit(:ballad_id, :artist, :title, :source, :release_year, :length, :have, :search, :page, :sort, :direction)
    end
end
