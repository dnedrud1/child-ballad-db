class BalladsController < ApplicationController

  before_action :authenticate_user!

  def index
    @songs = Song.order(:ballad_id)
    respond_to do |format|
      format.html
      format.csv { send_data @songs.to_csv }
      format.xls # { send_data @songs.to_csv( col_sep: "\t") }
      format.xlsx
    end
  end

  def new
    @song = Song.new
  end

  def create
    @song = Song.new(song_params)
    if @song.save
      flash[:success] = "Song created"
      redirect_to ballad_path(@song)
    else
      render 'new'
    end
  end

  def edit
    @song = Song.find(params[:id])
  end

  def update
    @song = Song.find(params[:id])
    if @song.update_attributes(song_params)
      flash[:success] = "Song updated"
      redirect_to ballad_path(@song)
    else
      render 'edit'
    end
  end

  def destroy
    @song = Song.find(params[:id])
    @song.destroy
    redirect_to admin_songs_data_path
  end

  def show
    @song = Song.find(params[:id])
  end

  def import
    Song.import(params[:file])
    redirect_to admin_music_data_path, notice: "Songs imported."
  end

  private

    def song_params
      params.require(:song).permit(:ballad_id, :title, :artist,
                                   :source, :release_year, :length, :have)
    end
end
