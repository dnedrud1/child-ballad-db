class StaticPagesController < ApplicationController
  before_action :disable_nav
  helper_method :sort_column, :sort_direction

  def home
  end

  def contact
  end

  def history
  end

  def links
  end

  def all
    @ballads = ChildBallad.all
    filteredSongs = song_params.has_key?(:ballad_id) && song_params[:ballad_id] != "" ? ChildBallad.find(song_params[:ballad_id]).songs : Song
    @songs = filteredSongs.search(song_params[:search]).order(sort_column + " " + sort_direction).paginate(:per_page => 50, :page => song_params[:page])
  end
  
  def ballads
    @ballads = ChildBallad.all
    @songs = song_params.has_key?(:ballad_id) ? ChildBallad.find(song_params[:ballad_id]).songs : []
  end

  private
  
    def sort_column
      Song.column_names.include?(song_params[:sort]) ? song_params[:sort] : "ballad_id"
    end
    
    def sort_direction
      %w[asc desc].include?(song_params[:direction]) ? song_params[:direction] : "asc"
    end

    def song_params
      params.permit(:ballad_id, :artist, :title, :source, :release_year, :length, :have, :search, :page, :sort, :direction)
    end

end
