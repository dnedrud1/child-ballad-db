class DataVisController < ApplicationController
    before_action :disable_nav
  
    def home
    end
  
    def popularity
      sql = "select cb.number, cb.name, COUNT(s.id) as ballad_count 
        from songs s 
        inner join child_ballads cb on cb.number = s.ballad_id 
        group by cb.number, cb.name 
        order by cb.number"
      @records_array = ActiveRecord::Base.connection.execute(sql)

      respond_to do |format|
        format.html
        format.json { render json: @records_array }
      end
    end
  
    def timeline
      sql = "select s.release_year, COUNT(s.id) as ballad_count 
        from songs s 
        where s.release_year is not null
        group by s.release_year 
        order by s.release_year"
      sql_array = ActiveRecord::Base.connection.execute(sql)
      @records_array = sql_array.select { |i| (1582..2500).include?(i['release_year'].to_i) && i['release_year'].length == 4 }

      respond_to do |format|
        format.html
        format.json { render json: @records_array }
      end
    end
  
    private
  
  end
  