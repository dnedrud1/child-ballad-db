class Song < ApplicationRecord

    validates :ballad_id, presence: true, numericality: true
    validates :artist, presence: true
    validates :title, presence: true
    validates :length, presence: false, numericality: true    

    belongs_to :child_ballad, foreign_key: "ballad_id", primary_key: "number", required: true

    def self.to_csv(options = {})
        CSV.generate(options) do |csv|
            csv << column_names
            all.each do |song|
                csv << song.attributes.values_at(*column_names)
            end
        end
    end

    def self.search(search)
        if search
            where('lower(artist) LIKE lower(?) or lower(title) LIKE lower(?) 
                or lower(source) LIKE lower(?)', 
                "%#{search}%", "%#{search}%", "%#{search}%")
        else
            where(nil)
        end
    end

end
