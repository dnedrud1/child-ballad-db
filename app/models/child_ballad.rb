class ChildBallad < ApplicationRecord
    has_many :songs, foreign_key: "ballad_id"
end
