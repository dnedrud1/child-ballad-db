class BalladImport
  include ActiveModel::Model

  attr_accessor :file, :success_count

  def initialize(attributes = {})
    attributes.each { |name, value| send("#{name}=", value) }
  end

  def persisted?
    false
  end

  def save
    @success_count = 0
    if imported_ballads.map(&:valid?).all?
      imported_ballads.each_with_index do |song, index|        
        begin
          song.save!
          @success_count += 1
        rescue => ex
          errors.add :base, "Row #{index+2}: #{ex.message}"
        end 
      end
      true
    else
      imported_ballads.each_with_index do |song, index|
        song.errors.full_messages.each do |message|
          errors.add :base, "Row #{index+2}: #{message}"
        end
      end
      false
    end
  end

  def imported_ballads
    @imported_ballads ||= load_imported_ballads
  end

  def load_imported_ballads
    spreadsheet = open_spreadsheet
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).map do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      song = Song.find_by_id(row["id"]) || Song.new
      song.attributes = row.to_hash.slice(*Song.column_names)
      song
    end
  end

  def open_spreadsheet
    case File.extname(file.original_filename)
    when ".csv" then Roo::CSV.new(file.path, csv_options: {encoding: "iso-8859-1:utf-8"})
    when ".xls" then Roo::Excel.new(file.path)
    when ".xlsx" then Roo::Excelx.new(file.path)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end
end