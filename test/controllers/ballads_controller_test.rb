require 'test_helper'

class BalladsControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get ballads_new_url
    assert_response :success
  end

  test "should get edit" do
    get ballads_edit_url
    assert_response :success
  end

  test "should get show" do
    get ballads_show_url
    assert_response :success
  end

end
